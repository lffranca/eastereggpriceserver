const app = require('../src');
const chai = require('chai');
const request = require('supertest');

const expect = chai.expect;

describe('API Tests', () => {
    it('should return version number', (done) => {
        request(app)
        .post('/api/v1/auth')
        .end((err, res) => {
            expect(res.body.token).to.be.ok;
            expect(res.body.user).to.be.ok;
            expect(res.statusCode).to.be.equal(200);
            done();
        });
    });
});