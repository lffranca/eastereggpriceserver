const express = require('express');
const app = express();

// Implement your services here //
app.use('/v1', require('../modules-v1'));

module.exports = app;
