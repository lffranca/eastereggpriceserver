const redisLib = require('redis');
const bluebird = require('bluebird');

const redisConfig = {
    "host": process.env.REDIS_HOST,
    "port": process.env.REDIS_PORT
};

bluebird.promisifyAll(redisLib.RedisClient.prototype);
bluebird.promisifyAll(redisLib.Multi.prototype);

module.exports = {
    redisClient: redisLib.createClient(redisConfig),
    redis: redisLib
};