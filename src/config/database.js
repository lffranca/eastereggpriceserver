const mongoose = require('mongoose');
const UserSchema = require('../schemas/User');
const EggSchema = require('../schemas/Egg');
const bcrypt = require('bcrypt');

mongoose.connect(process.env.MONGO_DB_URL);

const User = mongoose.model('User', UserSchema);
const Egg = mongoose.model('Egg', EggSchema);

User.find()
.then(async (data) => {
    if(data.length <= 0) {
        const password123 = await bcrypt.hash('123', 10);

        const users = [{
            firstName: 'Lucas',
            lastName: 'França',
            email: 'email@email.com',
            password: password123,
            active: true,
            avatar: 'https://cdn.iconscout.com/public/images/icon/free/png-512/avatar-user-hacker-3830b32ad9e0802c-512x512.png',
        }];

        return User.create(users)
    } else {
        return data;
    }
})
.then((resultCreate) => {
    console.log('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
    console.log('> USER CREATE: ', resultCreate);
    console.log('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
    return Egg.find();
})
.then(async (data) => {
    if(data.length <= 0) {
        const eggs = [{
            name: 'Nestlé Classic',
            brand: 'Nestlé',
            flavor: 'Chocolate ao leite',
            weight: 240,
            price: 35.00,
            image: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k',
        }, {
            name: 'Nestlé Classic',
            brand: 'Nestlé',
            flavor: 'Chocolate ao leite',
            weight: 240,
            price: 35.00,
            image: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k',
        }, {
            name: 'Nestlé Classic',
            brand: 'Nestlé',
            flavor: 'Chocolate ao leite',
            weight: 240,
            price: 35.00,
            image: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k',
        }, {
            name: 'Nestlé Classic',
            brand: 'Nestlé',
            flavor: 'Chocolate ao leite',
            weight: 240,
            price: 35.00,
            image: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k',
        }, {
            name: 'Nestlé Classic',
            brand: 'Nestlé',
            flavor: 'Chocolate ao leite',
            weight: 240,
            price: 35.00,
            image: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k',
        }, {
            name: 'Nestlé Classic',
            brand: 'Nestlé',
            flavor: 'Chocolate ao leite',
            weight: 240,
            price: 35.00,
            image: 'https://static.vix.com/pt/sites/default/files/styles/large/public/bdm/1-Nestle-Classic.jpg?itok=Kr5JEn-k',
        }];

        return Egg.create(eggs);
    } else {
        return data;
    }
})
.then((resultCreate) => {
    console.log('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
    console.log('> EGGS CREATE: ', resultCreate);
    console.log('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
})
.catch((error) => {
    console.error('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
    console.error('> USER AND EGGS CREATE ERROR: ', error);
    console.error('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
});

module.exports = {
    User,
    Egg,
    mongoose
};