const app = require('./app');

const PORT = process.env.NODE_PORT;

app.use('/api', require('./routes/api'));

module.exports = app.listen(PORT, () => {
    console.log('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
    console.log(`> API EASTER RUN: http://localhost:${PORT}`);
    console.log(`> ENV: ${process.env.NODE_ENV}`);
    console.log('> ------------------------------------------------------');
    console.log('> ------------------------------------------------------');
});