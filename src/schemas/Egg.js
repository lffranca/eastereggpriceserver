const {Schema, SchemaTypes} = require('mongoose')

const EggSchema = new Schema({
    name: SchemaTypes.String,
    brand: SchemaTypes.String,
    flavor: SchemaTypes.String,
    weight: SchemaTypes.Number,
    price: SchemaTypes.Number,
    image: SchemaTypes.String,
});

module.exports = EggSchema;