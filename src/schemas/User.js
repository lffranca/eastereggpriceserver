const {Schema, SchemaTypes} = require('mongoose')

const UserSchema = new Schema({
    firstName: SchemaTypes.String,
    lastName: SchemaTypes.String,
    email: SchemaTypes.String,
    password: SchemaTypes.String,
    active: SchemaTypes.Boolean,
    avatar: SchemaTypes.String,
});

module.exports = UserSchema;