const {redisClient} = require('../../config/cache');

module.exports = async (req, res, next) => {
    const {authorization} = req.headers;

    if(authorization) {
        try {
            const userString = await redisClient.getAsync(authorization);
            
            if(userString) {
                const user = JSON.parse(userString);

                req.user = user;

                next();
            } else {
                res.status(403).json({
                    error: {
                        message: 'Token inválido!'
                    }
                });
            }
        } catch (error) {
            res.status(403).json({
                error: {
                    message: 'Token inválido!'
                }
            });
        }
    } else {
        res.status(403).json({
            error: {
                message: 'Token inválido!'
            }
        });
    }
};