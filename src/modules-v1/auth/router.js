const {Router} = require('express');
const router = Router();

const {login, logout} = require('./controller');

router.post('/', login);
router.delete('/', logout);

module.exports = router;