const Joi = require('joi');
const {User} = require('../../config/database');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const {redisClient} = require('../../config/cache');

const login = async (req, res, next) => {
    const schema = Joi.object().keys({
        email: Joi.string()
            .regex(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            .required()
            .options({
                language: {
                    any: { 
                        empty: 'Email é obrigatório!'
                    },
                    string: {
                        regex: { 
                            base: 'Email é inválido!'
                        }
                    },
                    label: 'Email'
                }
            }),
        password: Joi.string()
            .required()
            .options({
                language: {
                    any: { 
                        empty: 'Senha é obrigatória!'
                    },
                    string: {
                        base: 'Senha precisa ser um texto!'
                    },
                    label: 'Senha'
                }
            })
    });

    const { error, value } = Joi.validate(req.body, schema);

    if(!error) {
        const {email, password} = value;

        try {
            const users = await User.findOne({ email });

            if(users) {
                const passwordHash = users.password;

                const passwordCompare = await bcrypt.compare(password, passwordHash);

                const token = crypto.randomBytes(64).toString('hex') + '.' + new Date().getTime();

                const saveTokenResult = await redisClient.setAsync(token, JSON.stringify(users));

                if(saveTokenResult === "OK") {
                    if(passwordCompare) {
                        res.status(200).json({
                            token,
                            users
                        });
                    } else {
                        res.status(400).json({
                            error: {
                                message: 'Email ou senha incorretos!'
                            }
                        });
                    }
                } else {
                    res.status(400).json({
                        error: {
                            message: 'Email ou senha incorretos!'
                        }
                    });    
                }
            } else {
                res.status(400).json({
                    error: {
                        message: 'Email ou senha incorretos!'
                    }
                });
            }
        } catch (error) {
            res.status(400).json({
                error: {
                    message: error.message
                }
            });
        }
    } else {
        if(error instanceof Error) {
            res.status(400).json({
                error: {
                    message: error.message
                }
            });
        } else {
            res.status(400).json(error);
        }
    }
}

const logout = async (req, res, next) => {
    const {authorization} = req.headers;

    if(authorization) {
        try {
            const userString = await redisClient.getAsync(authorization);

            if(userString) {
                const resultDelete = await redisClient.delAsync(authorization);

                res.status(200).json('OK');
            } else {
                res.status(200).json('OK');
            }
        } catch (error) {
            res.status(200).json(error.message);
        }
    } else {
        res.status(200).json('OK');
    }
}

module.exports = {
    login,
    logout
}
