const app = require('express')();
const authorization = require('./middlewares/authorization');

app.use('/auth', require('./auth/router'));

app.use('/easter-eggs', authorization, require('./easter-eggs/router'));

module.exports = app;