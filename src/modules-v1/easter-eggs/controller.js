const {Egg} = require('../../config/database');

const getAll = async (req, res, next) => {
    try {
        const eggs = await Egg.find();

        res.status(200).json({
            status: 200,
            data: eggs
        });
    } catch (error) {
        res.status(400).json({
            status: 400,
            error: {
                message: error.message
            }
        });
    }
}

const getById = async (req, res, next) => {
    try {
        const egg = await Egg.findById(req.params.id);

        if(egg) {
            res.status(200).json({
                status: 200,
                data: egg
            }); 
        } else {
            res.status(400).json({
                status: 400,
                error: {
                    message: 'Produto inválido!'
                }
            });
        }
    } catch (error) {
        res.status(400).json({
            status: 400,
            error: {
                message: error.message
            }
        });
    }
}

module.exports = {
    getAll,
    getById
}
