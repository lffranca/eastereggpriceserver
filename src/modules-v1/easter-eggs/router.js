const {Router} = require('express');
const router = Router();

const {getAll, getById} = require('./controller');

router.get('/', getAll);
router.get('/:id', getById);

module.exports = router;